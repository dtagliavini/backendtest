<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', static function (Request $request) {
    $fields = $request->validate([
        'email' => 'required|string|email',
        'password' => 'required'
    ]);
    $user = \App\Models\User::where('email', $fields['email'])->first();
    if (!$user || !Hash::check($fields['password'], $user->password)) {
        return response(['status' => false, 'message' => 'invalid email or password'], 401);
    }
    $token = $user->createToken('musixmatch')->plainTextToken;
    $response = [
        'status' => true,
        'message' => 'Login successful!',
        'data' => [
            'user' => $user,
            'token' => $token
        ]
    ];
    return response($response, 200);
});


Route::namespace('App\Http\Controllers')->middleware('auth:sanctum')->group(function () {
    Route::get('/user', static function (Request $request) {
        return $request->user();
    });
    Route::post('/crossing', 'CrossingController@create');
});

