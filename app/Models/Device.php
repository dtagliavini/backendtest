<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};

/**
 * App\Models\Device
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Crossing[] $crossings
 * @property DeviceVehicle[] $deviceVehicles
 * @property User $user
 * @property Toll[] $tolls
 * @property-read int|null $crossings_count
 * @property-read int|null $device_vehicles_count
 * @property-read int|null $tolls_count
 * @method static \Illuminate\Database\Eloquent\Builder|Device newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Device newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Device query()
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereUserId($value)
 * @mixin \Eloquent
 * @method static \Database\Factories\DeviceFactory factory(...$parameters)
 */
class Device extends Model
{
    use HasFactory;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'status', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return HasMany
     */
    public function crossings(): HasMany
    {
        return $this->hasMany('App\Models\Crossing');
    }

    /**
     * @return HasMany
     */
    public function deviceVehicles(): HasMany
    {
        return $this->hasMany('App\Models\DeviceVehicle');
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return HasMany
     */
    public function tolls(): HasMany
    {
        return $this->hasMany('App\Models\Toll');
    }
}
