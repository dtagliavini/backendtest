<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};

/**
 * App\Models\Vehicle
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Crossing[] $crossings
 * @property DeviceVehicle[] $deviceVehicles
 * @property Toll[] $tolls
 * @property User $user
 * @property-read int|null $crossings_count
 * @property-read int|null $device_vehicles_count
 * @property-read int|null $tolls_count
 * @method static \Database\Factories\VehicleFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereUserId($value)
 * @mixin \Eloquent
 */
class Vehicle extends Model
{
    use HasFactory;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'status', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return HasMany
     */
    public function crossings(): HasMany
    {
        return $this->hasMany('App\Models\Crossing');
    }

    /**
     * @return HasMany
     */
    public function deviceVehicles(): HasMany
    {
        return $this->hasMany('App\Models\DeviceVehicle');
    }

    /**
     * @return HasMany
     */
    public function tolls(): HasMany
    {
        return $this->hasMany('App\Models\Toll');
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\Models\User');
    }
}
