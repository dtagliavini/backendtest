<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\DeviceVehicle
 *
 * @property integer $id
 * @property integer $vehicle_id
 * @property integer $device_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Device $device
 * @property Vehicle $vehicle
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle query()
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereVehicleId($value)
 * @mixin \Eloquent
 */
class DeviceVehicle extends Model
{
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'device_vehicle';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['vehicle_id', 'device_id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return BelongsTo
     */
    public function device(): BelongsTo
    {
        return $this->belongsTo('App\Models\Device');
    }

    /**
     * @return BelongsTo
     */
    public function vehicle(): BelongsTo
    {
        return $this->belongsTo('App\Models\Vehicle');
    }
}
