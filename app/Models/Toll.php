<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Toll
 *
 * @property integer $id
 * @property string $crossing_code
 * @property integer $user_id
 * @property integer $device_id
 * @property integer $segment_id
 * @property integer $vehicle_id
 * @property string $calculation
 * @property float $price
 * @property string $started_at
 * @property string $ended_at
 * @property string $created_at
 * @property string $updated_at
 * @property Crossing $crossing
 * @property Device $device
 * @property Segment $segment
 * @property User $user
 * @property Vehicle $vehicle
 * @method static \Illuminate\Database\Eloquent\Builder|Toll newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Toll newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Toll query()
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereCalculation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereCrossingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereEndedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereSegmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereVehicleId($value)
 * @mixin \Eloquent
 */
class Toll extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['crossing_code', 'user_id', 'device_id', 'segment_id', 'vehicle_id', 'calculation', 'price', 'started_at', 'ended_at', 'created_at', 'updated_at'];

    /**
     * @return BelongsTo
     */
    public function crossing(): BelongsTo
    {
        return $this->belongsTo('App\Models\Crossing', 'crossing_code', 'code');
    }

    /**
     * @return BelongsTo
     */
    public function device(): BelongsTo
    {
        return $this->belongsTo('App\Models\Device');
    }

    /**
     * @return BelongsTo
     */
    public function segment(): BelongsTo
    {
        return $this->belongsTo('App\Models\Segment');
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return BelongsTo
     */
    public function vehicle(): BelongsTo
    {
        return $this->belongsTo('App\Models\Vehicle');
    }
}
