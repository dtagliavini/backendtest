<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Station
 *
 * @property integer $id
 * @property string $name
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Crossing[] $crossings
 * @property Segment[] $segmentsFrom
 * @property Segment[] $segmentsTo
 * @property-read int|null $crossings_count
 * @property-read int|null $segments_from_count
 * @property-read int|null $segments_to_count
 * @method static \Illuminate\Database\Eloquent\Builder|Station newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Station newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Station query()
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Station extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'status', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return HasMany
     */
    public function crossings(): HasMany
    {
        return $this->hasMany('App\Models\Crossing');
    }

    /**
     * @return HasMany
     */
    public function segmentsFrom(): HasMany
    {
        return $this->hasMany('App\Models\Segment', 'station_id1');
    }

    /**
     * @return HasMany
     */
    public function segmentsTo(): HasMany
    {
        return $this->hasMany('App\Models\Segment', 'station_id2');
    }
}
