<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Facades\Log;

/**
 * App\Models\Crossing
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $device_id
 * @property integer $vehicle_id
 * @property integer $station_id
 * @property string $code
 * @property string $direction
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Device $device
 * @property Station $station
 * @property User $user
 * @property Vehicle $vehicle
 * @property Toll[] $tolls
 * @property-read int|null $tolls_count
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing query()
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereDirection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereStationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereVehicleId($value)
 * @mixin \Eloquent
 */
class Crossing extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'device_id', 'vehicle_id', 'station_id', 'code', 'direction', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return void
     */
    protected static function booted(): void
    {
        static::created(static function ($crossing) {
            /**
             * create toll record upon exiting highway
             */
            if ($crossing->direction === "exit") {
                $crossings = Crossing::where('code', $crossing->code)->orderBy('created_at')->get();
                $startedAt = $endedAt = null;
                $stationId1 = $stationId2 = 0;
                foreach ($crossings as $model) {
                    if ($model->direction === "enter") {
                        $stationId1 = $model->station_id;
                        $startedAt = $model->created_at;
                    } elseif ($model->direction === "exit") {
                        $stationId2 = $model->station_id;
                        $endedAt = $model->created_at;
                    }
                }

                $query = Segment::where(static function ($query) use ($stationId1, $stationId2) {
                    $query->where([
                        ['station_id1', $stationId1],
                        ['station_id2', $stationId2]
                    ])->orWhere([
                        ['station_id1', $stationId2],
                        ['station_id2', $stationId1]
                    ])->where('status', 1);
                });
                $segment = $query->first();

                $calculator = new $segment->payment_method;
                $amount = $calculator::exec($segment->length);

                Toll::create([
                    'crossing_code' => $crossing->code,
                    'user_id' => $crossing->user_id,
                    'device_id' => $crossing->device_id,
                    'segment_id' => $segment->id,
                    'vehicle_id' => $crossing->vehicle_id,
                    'calculation' => $segment->payment_method,
                    'price' => $amount,
                    'started_at' => $startedAt,
                    'ended_at' => $endedAt,
                ]);
            }
        });
    }

    /**
     * @return BelongsTo
     */
    public function device(): BelongsTo
    {
        return $this->belongsTo('App\Models\Device');
    }

    /**
     * @return BelongsTo
     */
    public function station(): BelongsTo
    {
        return $this->belongsTo('App\Models\Station');
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return BelongsTo
     */
    public function vehicle(): BelongsTo
    {
        return $this->belongsTo('App\Models\Vehicle');
    }

    /**
     * @return HasMany
     */
    public function tolls(): HasMany
    {
        return $this->hasMany('App\Models\Toll', 'crossing_code', 'code');
    }
}
