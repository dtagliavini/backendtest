<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};

/**
 * App\Models\Segment
 *
 * @property integer $id
 * @property integer $station_id1
 * @property integer $station_id2
 * @property string $payment_method
 * @property float $length
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Station $stationFrom
 * @property Station $stationTo
 * @property Toll[] $tolls
 * @property-read int|null $tolls_count
 * @method static \Illuminate\Database\Eloquent\Builder|Segment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Segment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Segment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereStationId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereStationId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Segment extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['station_id1', 'station_id2', 'payment_method', 'length', 'status', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return BelongsTo
     */
    public function stationFrom(): BelongsTo
    {
        return $this->belongsTo('App\Models\Station', 'station_id1');
    }

    /**
     * @return BelongsTo
     */
    public function stationTo(): BelongsTo
    {
        return $this->belongsTo('App\Models\Station', 'station_id2');
    }

    /**
     * @return HasMany
     */
    public function tolls(): HasMany
    {
        return $this->hasMany('App\Models\Toll');
    }
}
