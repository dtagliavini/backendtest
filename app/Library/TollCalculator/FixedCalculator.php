<?php

namespace App\Library\TollCalculator;

class FixedCalculator implements TollCalculator
{

    /**
     * @inheritDoc
     */
    public static function exec(float $length): float
    {
        return env("TOLL_FIXED_KM_AMOUNT") * $length;
    }
}
