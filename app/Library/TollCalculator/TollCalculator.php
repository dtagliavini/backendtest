<?php

namespace App\Library\TollCalculator;

interface TollCalculator
{
    /**
     * @param float $length
     * @return float
     */
    public static function exec(float $length): float;
}
