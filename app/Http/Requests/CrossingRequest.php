<?php

namespace App\Http\Requests;

use App\Models\{Crossing, Device, Station, Vehicle};
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Log;


class CrossingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'user_id' => 'required|exists:App\Models\User,id',
        ];

        $input = $this->all();

        if ($input['direction'] === "enter") {
            $rules['code'] = 'required|uuid|unique:crossings,code';
        } else {
            $rules['code'] = 'required|uuid';
        }

        $rules['device_id'] = [
            'required',
            static function ($attribute, $value, $fail) use ($input) {
                $exists = Device::where('id', $value)
                    ->where('status', 1)
                    ->where('user_id', $input['user_id'])
                    ->exists();
                if (!$exists) {
                    $fail("The attribute {$attribute} is invalid");
                }
            }
        ];
        $rules['vehicle_id'] = [
            'required',
            static function ($attribute, $value, $fail) use ($input) {
                $exists = Vehicle::where('id', $value)
                    ->where('status', 1)
                    ->where('user_id', $input['user_id'])
                    ->exists();
                if (!$exists) {
                    $fail("The attribute {$attribute} is invalid");
                }
            }
        ];
        $rules['station_id'] = [
            'required',
            static function ($attribute, $value, $fail) {
                $exists = Station::where('id', $value)
                    ->where('status', 1)
                    ->exists();
                if (!$exists) {
                    $fail("The attribute {$attribute} is invalid");
                }
            }
        ];
        $rules['direction'] = [
            'required',
            'in:enter,exit',
            static function ($attribute, $value, $fail) use ($input) {
                $failed = false;
                switch ($value) {
                    case "enter":
                        break;
                    case "exit":
                        $enterNotExist = Crossing::where('code', $input['code'])
                            ->where('direction', 'enter')
                            ->doesntExist();

                        $exitExist = Crossing::where('code', $input['code'])
                            ->where('direction', 'exit')
                            ->exists();

                        $failed = $enterNotExist || $exitExist;
                        Log::info("", [$enterNotExist, $exitExist]);
                        break;
                    default:
                        $failed = true;
                }

                if ($failed) {
                    $fail("The attribute {$attribute} is invalid");
                }
            }
        ];


        return $rules;
    }

    protected function failedValidation(Validator $validator): void
    {
        throw new HttpResponseException(response()->json([
            'errors' => $validator->errors(),
            'status' => true
        ], 422));
    }

}
