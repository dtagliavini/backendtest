<?php

namespace App\Http\Controllers;

use App\Http\Requests\CrossingRequest;
use App\Models\Crossing;
use Illuminate\Support\Facades\Response;


class CrossingController extends Controller
{
    public function create(CrossingRequest $crossingRequest)
    {
        $input = $crossingRequest->all();
        $crossing = Crossing::create($input);
        return response()->json([
            'status' => true,
            'id' => $crossing->id
        ]);
    }

}
