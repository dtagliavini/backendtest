<?php

namespace App\Console\Commands;

use App\Models\{Bill, Toll};
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class MonthlyAmount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amount:calculation
        {user :  The id of the user or all }
        {month=current : Month of current year, numeric value, 1 is January and so on }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate monthly amount for all user or a singular ones, for a particular month. If bill it\'s present and not visible, it will be deleted.' . PHP_EOL .
    '  Example:' . PHP_EOL .
    '  php artisan amount:calculation all' . PHP_EOL .
    '  php artisan amount:calculation 2 10' . PHP_EOL .
    '  php artisan amount:calculation all 10';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userId = $this->argument('user');
        $monthInput = $this->argument('month');

        $now = Carbon::now();
        $month = $monthInput === 'current' ? $now->month : $monthInput;

        $rules = [
            'month' => ['integer', 'between:1,12']
        ];

        if ($userId !== "all") {
            $rules['user_id'] = ['exists:App\Models\User,id'];
        }

        $validator = Validator::make([
            'user_id' => $userId,
            'month' => $month
        ], $rules);

        if ($validator->fails()) {
            $this->info('Calculation not executed. See error messages below:');
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return Command::FAILURE;
        }

        $startDate = Carbon::now();
        $startDate->month = $month;
        $startDate->startOfMonth();
        $endDate = Carbon::now();
        $endDate->month = $month;
        $endDate->endOfMonth();

        $query = Toll::whereBetween('ended_at', [$startDate, $endDate]);
        if (!empty($userId) && $userId!=="all") {
            $query->where("user_id", $userId);
        }

        $groupedTolls = $query->orderBy('user_id')->orderBy('created_at')->get()->groupBy('user_id');

        $bar = $this->getOutput()->createProgressBar($groupedTolls->count());
        $format = $bar::getFormatDefinition('very_verbose');
        $format = "%message% \n{$format}";
        $bar->setFormat($format);
        $bar->setMessage("Start calculating bill");

        $billedAt = Carbon::now();
        $billedAt->month = $month;
        $billedAt->day = 1;
        $billedAt->hour = 0;
        $billedAt->minute = 0;
        $billedAt->second = 0;

        $groupedTolls->each(static function ($tolls, $user_id) use ($bar, $billedAt) {
            $amount = 0;
            $tolls->each(static function ($toll) use (&$amount) {
                $amount += $toll->price;
            });
            $bar->setMessage("creating bill for user {$user_id}");

            Bill::updateOrCreate([
                'user_id' => $user_id,
                'billed_at' => $billedAt
            ], [
                'price' => $amount,
                'visible' => 0,
            ]);
            $bar->advance();
            sleep(0.5);

        });
        $bar->finish();
        $this->getOutput()->writeln("");
        return Command::SUCCESS;
    }
}
