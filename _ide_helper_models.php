<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Bill
 *
 * @property integer $id
 * @property integer $user_id
 * @property float $price
 * @property boolean $visible
 * @property string $billed_at
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Bill newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bill newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bill query()
 * @method static \Illuminate\Database\Eloquent\Builder|Bill whereBilledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bill whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bill wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bill whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bill whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bill whereVisible($value)
 * @mixin \Eloquent
 */
	class Bill extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Crossing
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $device_id
 * @property integer $vehicle_id
 * @property integer $station_id
 * @property string $code
 * @property string $direction
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Device $device
 * @property Station $station
 * @property User $user
 * @property Vehicle $vehicle
 * @property Toll[] $tolls
 * @property-read int|null $tolls_count
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing query()
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereDirection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereStationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Crossing whereVehicleId($value)
 * @mixin \Eloquent
 */
	class Crossing extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Device
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Crossing[] $crossings
 * @property DeviceVehicle[] $deviceVehicles
 * @property User $user
 * @property Toll[] $tolls
 * @property-read int|null $crossings_count
 * @property-read int|null $device_vehicles_count
 * @property-read int|null $tolls_count
 * @method static \Illuminate\Database\Eloquent\Builder|Device newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Device newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Device query()
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereUserId($value)
 * @mixin \Eloquent
 * @method static \Database\Factories\DeviceFactory factory(...$parameters)
 */
	class Device extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\DeviceVehicle
 *
 * @property integer $id
 * @property integer $vehicle_id
 * @property integer $device_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Device $device
 * @property Vehicle $vehicle
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle query()
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeviceVehicle whereVehicleId($value)
 * @mixin \Eloquent
 */
	class DeviceVehicle extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Segment
 *
 * @property integer $id
 * @property integer $station_id1
 * @property integer $station_id2
 * @property string $payment_method
 * @property float $length
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Station $stationFrom
 * @property Station $stationTo
 * @property Toll[] $tolls
 * @property-read int|null $tolls_count
 * @method static \Illuminate\Database\Eloquent\Builder|Segment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Segment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Segment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereStationId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereStationId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Segment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Segment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Station
 *
 * @property integer $id
 * @property string $name
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Crossing[] $crossings
 * @property Segment[] $segmentsFrom
 * @property Segment[] $segmentsTo
 * @property-read int|null $crossings_count
 * @property-read int|null $segments_from_count
 * @property-read int|null $segments_to_count
 * @method static \Illuminate\Database\Eloquent\Builder|Station newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Station newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Station query()
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Station whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Station extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Toll
 *
 * @property integer $id
 * @property string $crossing_code
 * @property integer $user_id
 * @property integer $device_id
 * @property integer $segment_id
 * @property integer $vehicle_id
 * @property string $calculation
 * @property float $price
 * @property string $started_at
 * @property string $ended_at
 * @property string $created_at
 * @property string $updated_at
 * @property Crossing $crossing
 * @property Device $device
 * @property Segment $segment
 * @property User $user
 * @property Vehicle $vehicle
 * @method static \Illuminate\Database\Eloquent\Builder|Toll newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Toll newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Toll query()
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereCalculation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereCrossingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereEndedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereSegmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Toll whereVehicleId($value)
 * @mixin \Eloquent
 */
	class Toll extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property Bill[] $bills
 * @property Crossing[] $crossings
 * @property Device[] $devices
 * @property Toll[] $tolls
 * @property Vehicle[] $vehicles
 * @property-read int|null $bills_count
 * @property-read int|null $crossings_count
 * @property-read int|null $devices_count
 * @property-read int|null $tolls_count
 * @property-read int|null $vehicles_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Vehicle
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Crossing[] $crossings
 * @property DeviceVehicle[] $deviceVehicles
 * @property Toll[] $tolls
 * @property User $user
 * @property-read int|null $crossings_count
 * @property-read int|null $device_vehicles_count
 * @property-read int|null $tolls_count
 * @method static \Database\Factories\VehicleFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereUserId($value)
 * @mixin \Eloquent
 */
	class Vehicle extends \Eloquent {}
}

