<?php

namespace Tests\Feature;

use App\Models\{Bill, Toll};
use Database\Seeders\CrossingSeeder;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class BillTest extends TestCase
{
    /**
     * Test artisan calculation command
     *
     * @return void
     */
    public function test_bill_calculation(): void
    {
        $this->seed(CrossingSeeder::class);

        $month = Carbon::now()->month;
        $this->artisan("amount:calculation all {$month}")->assertSuccessful();
    }

    public function test_bill_amount() : void
    {
        $bill = Bill::first();
        $date = Carbon::parse($bill->billed_at);
        $toll = Toll::select([DB::raw("SUM(price) as total")])
            ->where('user_id', $bill->user_id)
            ->whereMonth('ended_at', $date->month)
            ->groupBy('user_id')
            ->first();
        $this->assertSame(round($bill->price, 2), round($toll->total, 2));

    }
}
