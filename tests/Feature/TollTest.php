<?php

namespace Tests\Feature;

use App\Models\{Crossing, Station, User};
use Illuminate\Support\Str;
use Illuminate\Testing\AssertableJsonString;
use Tests\TestCase;

class TollTest extends TestCase
{
    /**
     * Testing user login.
     *
     * @return AssertableJsonString
     * @throws \Throwable
     */
    public function test_login(): AssertableJsonString
    {
        $user = User::with(["devices", "vehicles"])->first();
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/login', [
            'email' => $user->email,
            'password' => 'password'
        ])->assertJsonStructure(['status', 'message', 'data' => ['user', 'token']]);

        return $response->decodeResponseJson();
    }

    /**
     * Log user entering highway
     *
     * @depends test_login
     * @param $response
     * @return AssertableJsonString
     * @throws \Throwable
     */
    public function test_entering_highway_success($response): AssertableJsonString
    {
        $data = $response->json('data');
        $userModel = User::with(["devices", "vehicles"])->find($data['user']['id']);
        $device = $userModel->devices()->first();
        $vehicle = $userModel->vehicles()->first();
        $stationFrom = Station::where("status", 1)->inRandomOrder()->limit(1)->first();

        $uuid = (string)Str::uuid();
        $insert = [
            'code' => $uuid,
            'user_id' => $userModel->id,
            'device_id' => $device->id,
            'vehicle_id' => $vehicle->id,
            'station_id' => $stationFrom->id,
            'direction' => 'enter'
        ];
        $response = $this->withHeaders([
            'Authorization' => "Bearer {$data['token']}",
            'Accept' => 'application/json'
        ])->post('/api/crossing', $insert);

        $response->assertStatus(200);

        return $response->decodeResponseJson();
    }

    /**
     * Log user exiting highway
     *
     * @depends test_login
     * @param $response
     * @return void
     */
    public function test_exiting_highway_success($response): void
    {
        $data = $response->json('data');

        $userModel = User::with(["devices", "vehicles"])->find($data['user']['id']);
        $device = $userModel->devices()->first();
        $vehicle = $userModel->vehicles()->first();

        /* retriving enter highway record */
        $entering = Crossing::where('user_id', $data['user']['id'])
            ->where('device_id', $device->id)
            ->where('vehicle_id', $vehicle->id)
            ->where('direction', 'enter')->orderBy('id', 'desc')->first();
        $stationTo = Station::where("status", 1)->inRandomOrder()->limit(1)->first();

        $insert = [
            'code' => $entering->code,
            'user_id' => $entering->user_id,
            'device_id' => $entering->device_id,
            'vehicle_id' => $entering->vehicle_id,
            'station_id' => $stationTo->id,
            'direction' => 'exit'
        ];
        $response = $this->withHeaders([
            'Authorization' => "Bearer {$data['token']}",
            'Accept' => 'application/json'
        ])->post('/api/crossing', $insert);

        $response->assertStatus(200);
    }

    /**
     * Test not authenticated access
     *
     * @param $response
     * @return void
     */
    public function test_not_authenticated_entering_highway(): void
    {
        $userModel = User::with(["devices", "vehicles"])->first();
        $device = $userModel->devices()->first();
        $vehicle = $userModel->vehicles()->first();
        $stationFrom = Station::where("status", 1)->inRandomOrder()->limit(1)->first();

        $uuid = (string)Str::uuid();
        $insert = [
            'code' => $uuid,
            'user_id' => $userModel->id,
            'device_id' => $device->id,
            'vehicle_id' => $vehicle->id,
            'station_id' => $stationFrom->id,
            'direction' => 'enter'
        ];
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/crossing', $insert);
        $response->assertUnauthorized();
    }

    /**
     * Test not valid input
     *
     * @depends test_login
     * @param $response
     * @return void
     */
    public function test_wrong_data_entering_highway($response): void
    {
        $data = $response->json('data');

        $insert = [
            'code' => "x",
            'user_id' => 1000,
            'device_id' => 1000,
            'vehicle_id' => 1000,
            'station_id' => 1000,
            'direction' => 'whatever'
        ];
        $response = $this->withHeaders([
            'Authorization' => "Bearer {$data['token']}",
            'Accept' => 'application/json'
        ])->post('/api/crossing', $insert);

        $response->assertJsonValidationErrors(array_keys($insert));
    }

}
