## Test

Software Engineer - Backend Test

You’ve been asked by a company that owns a motorway to build an automatic toll software
infrastructure for their users.

Users have to be registered in the system.

Each user can (chose A or B):
a) Buy one device which will be installed in the vehicle
b) Buy one or more devices which will be installed in their vehicles

Users can enter or exit the motorway from a network of 500 stations across the country.
To simplify let’s assume you can reach any destination from each station.

When you drive through each station the device communicates with the station and notifies
that you’re entering (or exiting) the motorway from that specific station.

The cost for the travel is (chose A or B):

```
a) a fix cost per KM based on the distance from station A to station B
b) a cost that can be custom configured for each segment of the motorway. A segment
is a path that connect two adjacent stations.
```

Chose A or B as your desired level of complexity and then:

1. Design the database scheme for the users / device
2. Design the database scheme for the network of stations and a way to store or
    calculate the cost between all possible paths
3. Design the database scheme for the logs of the entrances/exits each vehicle will
    trigger when travelling through a station.
4. Write an API to store the logs of the entrance/exit on the database
5. Write the code to calculate the monthly amount due (for each customer the sum of
    all paths they drove in a given month) for
    a. A given customer
    b. Each customer

**Language: PHP**

Design, document and write the code.
Build a unit test for the whole system.

Thank you for your time.


## Installing

- git clone https://dtagliavini@bitbucket.org/dtagliavini/backendTest.git
- Installing dependencies: cd backendTest && composer update --dev
- Copy configuration file: cp .env.example .env
- Create a db on mysql and a username/password to use it, configure it in .env file
- Generate key: php artisan key:generate
- Running migrations: php artisan migrate
- Populating db: php artisan db:seed DatabaseSeeder
- Running Feature test: php artisan test

## Route
- post /api/login = Route to authenticate user and to get back bearer token needed for next api calls
-- requested params = email, password
- post /api/crossing = Route to log vehicle entering or exiting highway
-- requested params = user_id, device_id, vehicle_id, station_id, direction (enter or exit)

N.B user_id is not checked against bearer token.

## Command
- amount:calculation = Calculate monthly amount for all user or a singular ones, for a particular month. If bill it's present it will be updated

## Test

### BillTest

There are two method:

 - **test_bill_calculation**
    -- Feed db using CrossingSeeder, insert from three to five path for each vehicle owned by a user filling with a randomic start and end station.
    -- Fill Bill table using console command that calculate monthly amount for each or all user.
 - **test_bill_amount**
 -- Get the first bill and compare the amount with the sum from toll table

### TollTest

There are five method:

- **test_login**
-- Authenticate user by email and password
- **test_entering_highway_success** -> depends on test_login
-- Log vehicle entering highway
- **test_exiting_highway_success** -> depends on test_login
-- Log vehicle leaving highway
- **test_not_authenticated_entering_highway**
-- Check for unauthenticated access
- **test_wrong_data_entering_highway**
-- Check for wrong input data