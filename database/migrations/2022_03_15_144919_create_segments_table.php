<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('segments', static function (Blueprint $table) {
            $table->id();
            $table->bigInteger('station_id1')->unsigned();
            $table->bigInteger('station_id2')->unsigned();
            $table->string('payment_method', 50)->comment('Function to use to calculate toll price');
            $table->float('length')->comment('Length in kilometers');
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->unique(["station_id1", "station_id2", "status"], 'segment_status_unique');

            $table->foreign('station_id1')
                ->on('stations')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('station_id2')
                ->on('stations')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('segments');
    }
};
