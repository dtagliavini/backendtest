<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('tolls', static function (Blueprint $table) {
            $table->id();
            $table->uuid('crossing_code');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('device_id')->unsigned();
            $table->bigInteger('segment_id')->unsigned();
            $table->bigInteger('vehicle_id')->unsigned();
            $table->string('calculation', 50)->comment('Function to use to calculate toll price');
            $table->double('price', 10, 2);
            $table->dateTime('started_at')->comment('Date when entered the highway');
            $table->dateTime('ended_at')->comment('Date when ended the highway')->index();
            $table->timestamps();

            $table->foreign('crossing_code')
                ->on('crossings')
                ->references('code')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('user_id')
                ->on('users')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('device_id')
                ->on('devices')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('segment_id')
                ->on('segments')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('vehicle_id')
                ->on('vehicles')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('tolls');
    }
};
