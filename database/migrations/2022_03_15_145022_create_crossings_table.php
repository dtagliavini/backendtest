<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('crossings', static function (Blueprint $table) {
            $table->id();
            $table->uuid('code')->index();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('device_id')->unsigned();
            $table->bigInteger('vehicle_id')->unsigned();
            $table->bigInteger('station_id')->unsigned();
            $table->string('direction', 10);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->on('users')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('device_id')
                ->on('devices')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('vehicle_id')
                ->on('vehicles')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('station_id')
                ->on('stations')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('crossings');
    }
};
