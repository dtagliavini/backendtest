<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class StationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        for ($station = 1; $station <= 32; $station++) {
            DB::table('stations')->upsert([
                'name' => "A{$station}",
                'status' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], ['name', 'status'], ['updated_at']);
        }
    }
}
