<?php

namespace Database\Seeders;

use App\Models\Crossing;
use App\Models\Station;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CrossingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $stations = Station::where('status', 1)->get();

        $users = User::all();

        $users->each(static function ($user, $key) use ($stations) {
            $vehicles = $user->vehicles()->get();

            $vehicles->each(static function ($vehicle, $key2) use ($stations, $user) {

                $device = $vehicle->deviceVehicles()->first();
                for ($c = 1; $c <= rand(3, 5); $c++) {
                    $stationFrom = $stations->random();
                    do {
                        $stationTo = $stations->random();
                    } while ($stationTo->id === $stationFrom->id);

                    $uuid = (string)Str::uuid();
                    $date = Carbon::now();

                    $date->day = rand(1, $date->daysInMonth);
                    $date->hour = rand(0, 23);
                    $date->minute = rand(0, 59);

                    Crossing::create([
                        'user_id' => $user->id,
                        'device_id' => $device->id,
                        'vehicle_id' => $vehicle->id,
                        'station_id' => $stationFrom->id,
                        'code' => $uuid,
                        'direction' => 'enter',
                        'created_at' => $date
                    ]);

                    Crossing::create([
                        'user_id' => $user->id,
                        'device_id' => $device->id,
                        'vehicle_id' => $vehicle->id,
                        'station_id' => $stationTo->id,
                        'code' => $uuid,
                        'direction' => 'exit',
                        'created_at' => $date->addMinutes(rand(10, 120))
                    ]);
                }
            });
        });
    }
}
