<?php

namespace Database\Seeders;

use App\Models\{DeviceVehicle, User, Device, Vehicle};
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        User::factory()->count(20)->create()->each(static function ($user) {
            $devices = rand(1, 5);
            Device::factory()->count($devices)->create(['user_id' => $user->id])->each(function ($device) use ($user) {
                $vehicle = Vehicle::factory()->create(['user_id' => $user->id]);
                DeviceVehicle::create(['vehicle_id' => $vehicle->id, 'device_id'=>$device->id]);
            });
        });
    }
}
