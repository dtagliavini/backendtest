<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class SegmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stations = DB::table('stations')->get();
        $stations->each(static function ($item, $key) use ($stations) {
            $others = $stations->filter(static function($item2, $key2) use ($key) {
                return $key2>$key;
            });

            $others->each(static function ($item3, $key3) use ($item) {
                DB::table('segments')->upsert([
                    'station_id1' => $item->id,
                    'station_id2' => $item3->id,
                    'payment_method' => 'App\Library\TollCalculator\FixedCalculator',
                    'length' => rand(10, 1200),
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ], ['station_id1', 'station_id2', 'status'], ['updated_at']);
            });
        });
    }
}
